from algebre.givens import Givens
from algebre.matrix import Matrix
from algebre.vector import Vector


class Qr(Matrix):
    def __init__(self, matrice):
        super().__init__(matrice)
        self._g = []
        self.factorise()
        self._y = None

    def factorise(self):
        for c in range(self.dimension - 1):
            for l in range(c + 1, self.dimension):
                if self[l][c] != 0:
                    g = Givens(c, l, self)
                    self._matrice = g * self
                    self._g.append(g)

    def solve_x(self, b):
        for i in self._g:
            b = i * b
        self._y = b

        x = [0 for i in range(self.dimension)]
        for i in range(self.dimension - 1, -1, -1):
            temp = 0
            for c in range(i + 1, self.dimension):
                temp += self.matrice[i][c] * x[c]
            x[i] = round((self._y[i] - temp) / self[i][i], 4)

        return Vector(x)

    def get_g(self):
        return self._g

    def get_y(self):
        return self._y

    g = property(get_g)
    y = property(get_y)
