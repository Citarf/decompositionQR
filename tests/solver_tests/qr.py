import math
import unittest

from algebre.givens import Givens
from algebre.matrix import Matrix
from algebre.vector import Vector
from solver.qr import Qr


class QrTest(unittest.TestCase):
    def setUp(self):
        self.M = Matrix([[6, 5, 0], [5, 1, 4], [0, 4, 3]])
        self.g01 = Givens(0, 1, self.M)

        self.A = Matrix([[1, 1, 0], [0, -1000, 1], [0, 0, 1]])
        self.B = Vector([0, 0, 5])

        self.C = Matrix([[1, 2, 3], [2, 7, 18], [4, 13, 38]])

        self.u = Vector([1, 2, 3])
        self.v = Vector([4, 5, 6])

    def test_coeff_a(self):
        self.assertEqual(self.g01._a, 6)

    def test_coeff_b(self):
        self.assertEqual(self.g01._b, 5)

    def test_coeff_r(self):
        self.assertEqual(round(math.sqrt(math.pow(self.g01._a, 2) + math.pow(self.g01._b, 2)), 4), 7.8102)

    def test_coeff_c(self):
        self.assertEqual(round(self.g01.c, 4), 0.7682)

    def test_coeff_s(self):
        self.assertEqual(round(self.g01.s, 4), -0.6402)

    def test_mul_givens_matrix(self):
        self.assertEqual((self.g01 * self.M).matrice, [[7.810249675906654, 4.48129079765136, 2.5607375986579197],
                                                       [-4.440892098500626e-16, -2.4327007187250236,
                                                        3.0728851183895034],
                                                       [0, 4, 3]])

    def test_factor(self):
        self.assertEqual(Qr(self.C).matrice.matrice, [[4.582575694955841, 14.838816536047482, 41.67961703507455],
                                                      [0.0, 1.3451854182690988, 5.593139370697833],
                                                      [0.0, -1.3877787807814457e-17, 2.919985580353722]])


unittest.main()
