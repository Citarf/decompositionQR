import unittest

from algebre.matrix import Matrix
from algebre.vector import Vector


class LuTest(unittest.TestCase):
    def setUp(self):
        self.m = Matrix([[1, 4, 7], [2, 5, 8], [3, 6, 9]])
        self.n = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        self.c = Matrix([[2, 4, 4], [1, 3, 1], [1, 5, 6]])

        self.A = Matrix([[1, 1, 0], [0, -1000, 1], [0, 0, 1]])
        self.B = Vector([0, 0, 5])

        self.u = Vector([1, 2, 3])
        self.v = Vector([4, 5, 6])

    def test_crout(self):
        l, u = self.c._crout()
        self.assertEqual((l * u).matrice, self.c.matrice)

    def test_solve(self):
        self.assertEqual(self.A.solve(self.B).vector, [-0.005, 0.005, 5.0])

    def test_det(self):
        self.assertEqual(self.c.det, 14.0)


unittest.main()
