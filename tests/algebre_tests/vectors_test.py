import math
import unittest

from algebre.matrix import Matrix
from algebre.vector import Vector


class VectorTest(unittest.TestCase):
    def setUp(self):
        self.m = Matrix([[1, 4, 7], [2, 5, 8], [3, 6, 9]])
        self.n = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        self.c = Matrix([[2, 4, 4], [1, 3, 1], [1, 5, 6]])

        self.A = Matrix([[1, 1, 0], [0, -1000, 1], [0, 0, 1]])
        self.B = Vector([0, 0, 5])

        self.u = Vector([1, 2, 3])
        self.v = Vector([4, 5, 6])

    def test_produit_matrice_vecteur(self):
        # self.assertEqual(self.m.mul(self.u).vector, [30, 36, 42])
        self.assertEqual((self.m * self.u).vector, [30, 36, 42])

    def test_produit_vecteur_vecteur(self):
        self.assertEqual((self.u * self.v).vector, [-3, 6, -3])

    def test_calcul_norme(self):
        self.u = Vector([5, 3, math.sqrt(2)])
        self.assertEqual(self.u.norm, 6)

    def test_normalize(self):
        # self.assertEqual(Vector.normalize(self.u).vector, [0.26726, 0.53452, 0.80178])
        self.assertEqual(Vector.normalize(self.u).vector,
                         [0.2672612419124244, 0.53452248382484880, 0.80178372573727320])

    def test_dot(self):
        self.assertEqual(self.u.dot(self.v), 32)

    def test_produit_scalaire(self):
        self.assertEqual(Vector.dot(self.u, self.v), 32)

unittest.main()