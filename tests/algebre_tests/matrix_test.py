import math
import unittest

from algebre.matrix import Matrix
from algebre.vector import Vector


class MatrixTest(unittest.TestCase):
    def setUp(self):
        self.m = Matrix([[1, 4, 7], [2, 5, 8], [3, 6, 9]])
        self.n = Matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        self.c = Matrix([[2, 4, 4], [1, 3, 1], [1, 5, 6]])

        self.A = Matrix([[1, 1, 0], [0, -1000, 1], [0, 0, 1]])
        self.B = Vector([0, 0, 5])

        self.u = Vector([1, 2, 3])
        self.v = Vector([4, 5, 6])

    def test_produit_matriciel(self):
        # self.assertEqual(self.m.mul(self.n).matrice, [[66, 78, 90], [78, 93, 108], [90, 108, 126]])
        self.assertEqual((self.m * self.n).matrice, [[66, 78, 90], [78, 93, 108], [90, 108, 126]])

    def test_eye(self):
        self.assertEqual(Matrix.eye(3).matrice, [[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    def test_transpose(self):
        b = Matrix([[1, 4, 7], [2, 5, 8], [3, 6, 9]])
        result = b.transpose()
        self.assertEqual(result.matrice, self.n.matrice)

    def test_norme_frobenius(self):
        self.assertEqual(self.m.fnorm, math.sqrt(285))


unittest.main()
