from algebre.matrix import Matrix
from algebre.vector import Vector
from solver.qr import Qr

A = Matrix([[1, 2, 3], [2, 7, 18], [4, 13, 38]])
C = Matrix([[0, 0, 1], [0, -1000, 1], [1, 1, 0]])
M = Matrix([[0, 0, 0, 1, 0], [0, -1000, 0, 1, -1], [0, 0, -1000, 0, 1], [1, 1, 0, 0, 0], [0, -1, 1, 0, 0]])

d = Vector([5, 0, 0])
b = Vector([5, 0, 0, 0, 0])

x1 = Qr(M).solve_x(b)
print(x1.vector)
print((M * x1).vector)
