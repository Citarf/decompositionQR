from algebre.matrix import Matrix
from algebre.vector import Vector
from solver.qr import Qr

M = Matrix([[0, 0, 0, 1, 0], [0, -1000, 0, 1, -1], [0, 0, -1000, 0, 1], [1, 1, 0, 0, 0], [0, -1, 1, 0, 0]])
b = Vector([5, 0, 0, 0, 0])

x = Qr(M).solve_x(b)

print("x: " + str(x))
print("M*x: " + str((M * x)))
