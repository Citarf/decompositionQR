import math
import random

from algebre.vector import Vector


class Matrix:
    def __init__(self, matrice):

        self._matrice = matrice
        self._dimension = len(matrice)

    def __len__(self):
        return self._dimension

    def __getitem__(self, item):
        return self._matrice[item]

    def __str__(self):
        result = ""
        for i in range(self._dimension):
            result += str(self._matrice[i]) + "\n"
        return result

    def __iter__(self):
        return iter(self._matrice)

    def __mul__(self, w):
        """
        Calcule le produit matrice/vecteur ou matrice/matrice

        :param w:
        :return:
        """
        result = []
        if type(w) == Vector:
            for i in range(self._dimension):
                coefficient = 0
                for j in range(self._dimension):
                    coefficient += self._matrice[i][j] * w[j]
                result.append(coefficient)
            return Vector(result)

        elif type(w) == Matrix:
            for i in range(self._dimension):
                result.append([])
                for j in range(self._dimension):
                    coefficient = 0
                    for k in range(self._dimension):
                        coefficient += self._matrice[i][k] * w[k][j]
                    result[i].append(coefficient)
            return Matrix(result)

        # elif type(w) == Givens:
        #     result = self.clone()
        #     for c in range(self._dimension):
        #         result[w.i][c] = w.c * self._matrice[w.i][c] - w.s * self._matrice[w.j][c]
        #         result[w.j][c] = w.s * self._matrice[w.i][c] + w.c * self._matrice[w.j][c]
        #     return result

        else:
            raise TypeError("Error 1")  # Espace non matriciel

    def clone(self):
        result = Matrix.zeros(self._dimension)
        for i in range(self._dimension):
            for j in range(self._dimension):
                result[i][j] = self[i][j]
        return result

    def _trace(self):
        """
        Calcule la trace de la matrice.

        :return:
        """
        trace = 0
        for i in range(self._dimension):
            trace += self.matrice[i][i]
        return trace

    def _fNorm(self):
        """
        Calcule la norme de Frobenius d'une matrice.
        :return:
        """
        result = 0
        for i in range(self._dimension):
            for j in range(self._dimension):
                result += self[i][j] ** 2
        return math.sqrt(result)

    def _det(self):
        try:
            l, u = self._crout()
        except ZeroDivisionError:
            print("Error 2")  # Matrice non inversible.
            return 0
        detL = 1
        detU = 1
        for i in range(len(l)):
            detL *= l[i][i]
            detU *= u[i][i]
        return detL * detU

    def transpose(self):
        """
        Transpose la matrice.

        :return:
        """
        matriceT = []
        for i in range(self._dimension):
            matriceT.append([])
            for j in range(self._dimension):
                matriceT[i].append(self.matrice[j][i])
        return Matrix(matriceT)

    def _crout(self):
        """
        Factorise une matrice en décomposition LU.

        :return:
        """
        l = self.zeros(self._dimension)
        u = self.eye(self._dimension)

        for i in range(self._dimension):
            for j in range(i, self._dimension, 1):
                temp1 = 0
                for c in range(self._dimension):
                    if c != i:
                        temp1 += l[j][c] * u[c][i]
                l[j][i] = (self[j][i] - temp1)

            for j in range(i, self._dimension, 1):
                temp2 = 0
                for c in range(self._dimension):
                    if c != i:
                        temp2 += l[i][c] * u[c][j]

                u[i][j] = (self[i][j] - temp2) * (1 / l[i][i])

        return l, u

    def solve(self, b, p=3):
        if len(self[0]) != len(b):
            print("Error 3")  #Système incompatible.
            return None
        try:
            l, u = self._crout()
        except ZeroDivisionError:
            print("Error 2")  #Matrice non inversible.
            return None
        y = self._solveY(l, b)
        return self._solveX(u, y, p)

    @staticmethod
    def _solveY(l, b):
        y = [0] * len(l)
        for i in range(len(l)):
            temp = 0
            for c in range(0, i):
                temp += l[i][c] * y[c]
            y[i] = (b[i] - temp) * (1 / l[i][i])
        return Vector(y)

    @staticmethod
    def _solveX(u, y, p=3):
        x = [0] * len(u)
        for i in range(len(u) - 1, -1, -1):
            temp = 0
            for c in range(i + 1, len(u)):
                temp += u[i][c] * x[c]
            x[i] = round(y[i] - temp, p)

        return Vector(x)

    @staticmethod
    def zeros(n):
        """
        Génère une matrice carrée nulle de taille n.

        :param n: Taille de la matrice
        :return: Objet de type Matrice
        """
        result = []
        for i in range(n):
            result.append([])
            for j in range(n):
                result[i].append(0)

        return Matrix(result)

    @staticmethod
    def eye(n):
        """
        Génère une matrice identité de taille n.

        :param n: Taille de la matrice identité,
        :return: Objet de type matrice de taille n.
        """
        result = []
        for i in range(n):
            result.append([])
            for j in range(n):
                if j != i:
                    result[i].append(0)
                else:
                    result[i].append(1)
        return Matrix(result)

    @staticmethod
    def random(row, column):
        """
        Génère une matrice aléatoire de taille n.

        :param row: Nombre de ligne de la matrice
        :param column: Nombre de colonne de la matrice
        :return: Objet de type matrice de taille (row, column)
        """
        result = []

        for i in range(row):
            result.append([])
            for j in range(column):
                result[i].append(random.randint(0, 100))

        return Matrix(result)

    def _getMatrice(self):
        return self._matrice

    def _getDimension(self):
        return self._dimension

    def _getTrace(self):
        return self._trace()

    def _getFNorm(self):
        return self._fNorm()

    def _getDet(self):
        return self._det()

    matrice = property(_getMatrice)
    dimension = property(_getDimension)
    trace = property(_getTrace)
    fnorm = property(_getFNorm)
    det = property(_getDet)
