import math
import random


class Vector:
    def __init__(self, v):
        self._v = v
        self._norm = 0
        self._dimension = len(v)

        for i in self._v:
            self._norm += i * i
        self._norm = math.sqrt(self._norm)

    def __len__(self):
        return self._dimension

    def __getitem__(self, item):
        return self._v[item]

    def __str__(self):
        return str(self._v)

    def __iter__(self):
        return iter(self._v)

    # print(u[1] * v[2] - u[2] * v[1])
    # print(u[2] * v[3] - u[3] * v[2])
    # print(u[3] * v[0] - u[0] * v[3])
    # print(u[0] * v[1] - u[1] * v[0])

    def __mul__(self, w):
        if type(w) == Vector:
            result = []
            j = 2
            for i in range(1, self._dimension):
                result.append(self[i] * w[j] - self[j] * w[i])
                if j == self._dimension - 1:
                    j = 0
                else:
                    j += 1
            result.append(self[0] * w[1] - self[1] * w[0])
            return Vector(result)
        # if type(w) == Matrice:
        else:
            raise TypeError("Multiplication à gauche non implémentée.")

    def clone(self):
        result = Vector.zeros(self._dimension)
        for i in range(self._dimension):
            result[i] = self[i]
        return result

    def normalize(self):
        if self._norm == 0:
            return 0
        return Vector([(1 / self._norm) * i for i in self._v])

    def dot(self, w):
        if self._dimension != w.dimension:
            return None
        result = 0
        for i in range(self._dimension):
            result += w[i] * self._v[i]
        return result

    def _getNorm(self):
        return self._norm

    def _getVector(self):
        return self._v

    def _getDimension(self):
        return self._dimension

    @staticmethod
    def random(n):
        result = []
        for i in range(n):
            result.append(random.randint(0, 100))
        return Vector(result)

    @staticmethod
    def zeros(n):
        return [0 for i in range(n)]

    vector = property(_getVector)
    norm = property(_getNorm)
    dimension = property(_getDimension)
