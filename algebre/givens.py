import math

from algebre.vector import Vector


class Givens:
    def __init__(self, i, j, mat):
        """
        A matrix givens class with just revelant coefficients.

        :param i: Coefficient column
        :param j: Coefficient row
        :param mat: Matrix
        """
        self._i = i
        self._j = j
        self._a = mat[i][i]
        self._b = mat[j][i]
        r = math.sqrt(math.pow(self._a, 2) + math.pow(self._b, 2))
        self._c = self._a / r
        self._s = -self._b / r

    def __mul__(self, other):
        """
        Multiply a Givens matrix with a matrix.

        :param other: Matrix object
        :return: Matrix object with zero at column: i, and row: j
        """
        if type(other) == Vector:
            result = other.clone()
            result[self._i] = self._c * other.vector[self._i] - self._s * other.vector[self._j]
            result[self._j] = self._s * other.vector[self._i] + self._c * other.vector[self._j]
            return Vector(result)
        else:
            result = other.clone()
            for c in range(other.dimension):
                result[self._i][c] = self._c * other.matrice[self._i][c] - self._s * other.matrice[self._j][c]
                result[self._j][c] = self._s * other.matrice[self._i][c] + self._c * other.matrice[self._j][c]
            return result

    def __str__(self):
        return "G[" + str(self._i) + "][" + str(self._j) + "]"

    def _get_i(self):
        return self._i

    def _get_j(self):
        return self._j

    def _get_s(self):
        return self._s

    def _get_c(self):
        return self._c

    i = property(_get_i)
    j = property(_get_j)
    s = property(_get_s)
    c = property(_get_c)
